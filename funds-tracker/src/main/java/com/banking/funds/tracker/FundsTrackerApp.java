package com.banking.funds.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FundsTrackerApp {
	public static void main(String[] args) {
		SpringApplication.run(FundsTrackerApp.class, args);
	}
}
